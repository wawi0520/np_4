
#ifndef _TOUR_PACKET_H_
#define _TOUR_PACKET_H_
#include "Config.h"
#include "NpType.h"

typedef struct
{
    char nodeList[MAX_TOUR_NODE][IP_SIZE];
    NpUint32 indexToCurrentNode;
    NpUint32 totalNumberOfNodes;
    char multicastIPAddr[IP_SIZE];
    NpPort multicastPort;
} TourPacket;

void InitTourPacket( TourPacket *packet );

void BuildTourPacket
    (
    TourPacket *packet,
    char *thisNodeIPAddr,
    char **visitNodeList,
    const char numOfVisitNode,
    char *multicastIPAddr,
    NpPort multicastPort
    );

void FindPreviousNode
    (
    char *previousIPAddr,
    char **nodeList,
    char *thisIPAddr
    );

#endif

