#include "CommonTasks.h"
#include "NetworkInterface.h"

const NpInt32 MAX_CONNECTION = 128;

//=============================================================================
void BuildHwAddr
    (
    HwAddr *hwAddr,
    NpInt32 sll_ifindex,
    unsigned short sll_hatype,
    NpUchar sll_halen,
    NpUchar *sll_addr
    )
{
    hwAddr->sll_ifindex = sll_ifindex;
    hwAddr->sll_hatype = sll_hatype;
    hwAddr->sll_halen = sll_halen;
    bzero( &hwAddr->sll_addr, 8 );
    bcopy( sll_addr, hwAddr->sll_addr, MAC_SIZE );
}

//=============================================================================
void DebugHwAddr( HwAddr *hwAddr )
{
    char presentationMacAddr[MAC_PRESENTATION_SIZE];
    ToMACPresentationFormat( presentationMacAddr, hwAddr->sll_addr );
    printf("====================HwAddr======================\n");
    printf( "sll_ifindex: %d\n", hwAddr->sll_ifindex );
    printf( "sll_hatype: %u\n", hwAddr->sll_hatype );
    printf( "sll_halen : %c\n", hwAddr->sll_halen );
    printf( "sll_addr: %s\n", presentationMacAddr );
    printf( "===============================================\n" );
}

//=============================================================================
char *GetCanonicalIPAddr( NeworkInterfacesTable *table, const NpUint32 count )
{
    NpUint32 i = 0;
    for( ; i < count; ++i )
    {
        if( 0 == bcmp( table[i].name, "eth0", strlen( "eth0" ) ) )
        {
            return table[i].IPAddr;
        }
    }
}

//=============================================================================
char *GetMainMacAddr( NeworkInterfacesTable *table, const NpUint32 count )
{
    NpUint32 i = 0;
    for( ; i < count; ++i )
    {
        if( 0 == bcmp( table[i].name, "eth0", strlen( "eth0" ) ) )
        {
            return table[i].hwAddr;
        }
    }
}

//=============================================================================
const NpInt32 GetMainInterface( NeworkInterfacesTable *table, const NpUint32 count )
{
    NpUint32 i = 0;
    for( ; i < count; ++i )
    {
        if( 0 == bcmp( table[i].name, "eth0", strlen( "eth0" ) ) )
        {
            return table[i].interfaceIndex;
        }
    }
}

//=============================================================================
NpInt32
MySelect
    (
    NpInt32 nfds,
    fd_set *readfds,
    fd_set *writefds,
    fd_set *exceptfds,
    struct timeval *timeout
    )
{
    NpInt32 n;
    if ( (n = select(nfds, readfds, writefds, exceptfds, timeout)) < 0)
    {
        if( EINTR == errno )
        {
            //err_msg( "Non-fatal select error\n" );
        }
        else
        {
            err_quit( "select error" );
        }
    }
    return(n);      /* can return 0 on timeout */
}

//=============================================================================
void IPAddrToHostName( char *hostname, char *IPAddr )
{
    bzero( hostname, MAX_LINE );

    struct hostent *host;
    struct in_addr addr;
    bzero( &addr, sizeof( addr ) );

    Inet_pton( AF_INET, IPAddr, &addr );
    host = gethostbyaddr((char *) &addr, 4, AF_INET);

    bcopy( host->h_name, hostname, strlen( host->h_name ) ) ;
}

//=============================================================================
void SetupListeningSocket( NpInt32 *listenFd )
{
    *listenFd = Socket( AF_LOCAL, SOCK_STREAM, 0 );
    struct sockaddr_un servaddr;
    bzero( &servaddr, sizeof( servaddr ) );
    servaddr.sun_family = AF_LOCAL;
    bcopy( ARP_PATH1, servaddr.sun_path, strlen( ARP_PATH1 ) );
    Bind
        (
        *listenFd,
        (SA *) &servaddr,
        sizeof( servaddr )
        );
    Listen( *listenFd, MAX_CONNECTION );
}

