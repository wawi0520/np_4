#include "TourPacket.h"
#include "unp.h"
#include "Config.h"
#include "NpType.h"

//=============================================================================
void InitTourPacket( TourPacket *packet )
{
    NpUint32 i = 0;
    for( ; i < MAX_TOUR_NODE; ++i )
    {
        bzero( &packet->nodeList[i],  IP_SIZE );
    }
    packet->indexToCurrentNode = 0;
    packet->totalNumberOfNodes = 0;
    bzero( &packet->multicastIPAddr,  IP_SIZE );
    packet->multicastPort = 0;
}

//=============================================================================
void BuildTourPacket
    (
    TourPacket *packet,
    char *thisNodeIPAddr,
    char **visitNodeList,
    const char numOfVisitNode,
    char *multicastIPAddr,
    NpPort multicastPort
    )
{
    bcopy( thisNodeIPAddr, packet->nodeList[0], strlen( thisNodeIPAddr ) );
    NpUint32 i = 0;
    for( ; i < numOfVisitNode; ++i )
    {
        // First, convert the host name to the IP address. Skip the first
        // element of node lists becuase it is actually the executable name.
        struct hostent *pRemoteHost = gethostbyname( visitNodeList[i+1] );
        if( NULL == pRemoteHost )
        {
            err_sys( "Error: gethostbyname fails..." );
        }
        char IPAddr[IP_SIZE];
        bzero( &IPAddr, IP_SIZE );
        Inet_ntop(AF_INET, *(pRemoteHost->h_addr_list), IPAddr, sizeof( IPAddr ) );

        // Second, copy the converted IP Address to our tour packet
        bcopy( IPAddr, packet->nodeList[i+1], strlen( IPAddr ) );
    }
    packet->indexToCurrentNode = 0;
    packet->totalNumberOfNodes = numOfVisitNode + 1; // +1, plus this node
    bcopy( multicastIPAddr, packet->multicastIPAddr, strlen( multicastIPAddr ) );
    packet->multicastPort = multicastPort;
}

//=============================================================================
void DebugTourPacket( TourPacket *packet )
{
    printf("==================Tour Packet===================\n");
    printf( "Node Lists:\n" );
    NpUint32 i = 0;
    for( ; i < packet->totalNumberOfNodes; ++i )
    {
        printf( "\tNode%u: %s(%d)\n", i+1, packet->nodeList[i], (NpInt32)strlen(packet->nodeList[i]) );
    }
    printf( "Current index: %u\n", packet->indexToCurrentNode );
    printf( "Multicast IP Address : %s(%d)\n", packet->multicastIPAddr, (NpInt32)strlen(packet->multicastIPAddr) );
    printf( "Multicast port: %u\n", packet->multicastPort );
    printf( "===============================================\n" );
}

//=============================================================================
void FindPreviousNode
    (
    char *previousIPAddr,
    char **nodeList,
    char *thisIPAddr
    )
{
    NpUint32 i = 0;
    for( ; i < MAX_TOUR_NODE; ++i )
    {
        if( 0 == bcmp( nodeList[i], thisIPAddr, strlen( thisIPAddr ) ) )
        {
            if( 0 == i )
            {
                err_sys( "The first visited node should not be the starting node. Exit..." );
            }
            bcopy( nodeList[i-1], previousIPAddr, IP_SIZE );
            return;
        }
    }

    return;
}

