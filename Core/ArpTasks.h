#ifndef _ARP_TASKS_H_
#define _ARP_TASKS_H_
#include "ArpPacket.h"
#include "NetworkInterface.h"
#include "NpType.h"

NpBool FloodARPRequestMsg
    (
    const NpInt32 fd,
    char *IPAddr,
    NeworkInterfacesTable *ifTable,
    const NpUint32 numOfInterfaces
    );

NpBool ReplyARPRequestMsg
    (
    const NpInt32 fd,
    ARPPacket *packet,
    NpInt32 sll_ifindex
    );

#endif
