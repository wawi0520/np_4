#include <linux/ip.h>
#include <stdlib.h>
#include "TourTasks.h"
#include "unp.h"
#include "TourPacket.h"

//=============================================================================
void SetupRtSocket( NpInt32 *fd )
{
    const NpInt32 on = 1;

    *fd = Socket( AF_INET, SOCK_RAW, RT_PROTOCOL );
    Setsockopt( *fd, IPPROTO_IP, IP_HDRINCL, &on, sizeof( on ) );
    Setsockopt( *fd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof( on ) );
}

//=============================================================================
void SendTourPacket( const NpInt32 fd, TourPacket *packet )
{
    // Adapted from http://stackoverflow.com/questions/13620607/creating-ip-network-packets
    // and http://aschauf.landshut.org/fh/linux/udp_vs_raw/ch01s03.html#id2766267

    // IP header reference: http://lxr.free-electrons.com/source/include/uapi/linux/ip.h#L85

    struct sockaddr_in srcAddr;
    struct sockaddr_in destAddr;
    struct iphdr *ipHeader;  // IP header
    void* buffer = (void *)malloc( sizeof( struct iphdr ) + sizeof( TourPacket ) );
    ipHeader = (struct iphdr *)buffer;
    unsigned char *payload = buffer + sizeof(struct iphdr);

    // Task 1: fill header
    ipHeader->ihl = 5;
    ipHeader->version = 4;
    ipHeader->tot_len = sizeof( struct iphdr ) + sizeof( TourPacket );
    ipHeader->id = htons( IDENTIFICATION );
    ipHeader->ttl = TTL;
    ipHeader->protocol = RT_PROTOCOL;
    ipHeader->check = in_cksum( (unsigned short *)ipHeader, sizeof( struct iphdr ) );

    srcAddr.sin_family= AF_INET;
    srcAddr.sin_addr.s_addr = inet_addr( packet->nodeList[ packet->indexToCurrentNode ]);
    ipHeader->saddr = srcAddr.sin_addr.s_addr;

    destAddr.sin_family = AF_INET;
    destAddr.sin_addr.s_addr = inet_addr( packet->nodeList[ packet->indexToCurrentNode + 1 ]);
    ipHeader->daddr = destAddr.sin_addr.s_addr;
    // Increment the index before sending out
    ++packet->indexToCurrentNode;

    // Task 2: fill payload (our own data)
    bcopy( packet, payload, sizeof( TourPacket ));

    // Task 3: send out
    Sendto
        (
        fd,
        buffer,
        sizeof( TourPacket ) + sizeof( struct iphdr ),
        0,
        (SA *) &destAddr,
        sizeof( struct sockaddr_in )
        );

    if( NULL != buffer )
    {
        free( buffer );
    }
}

//=============================================================================
NpBool areq
    (
    struct sockaddr *sa,
    socklen_t sockaddrlen,
    HwAddr *hwAddr
    )
{
    NpBool success = TRUE;
    char IPAddr[IP_SIZE];
    bzero( &IPAddr, IP_SIZE );
    char *pStr = Sock_ntop_host( sa, sizeof(struct sockaddr) );
    bcopy( pStr, IPAddr, strlen( pStr ) );

    struct sockaddr_un servaddr;
    bzero( &servaddr, sizeof( servaddr ) );
    NpInt32 sockFd = Socket( AF_LOCAL, SOCK_STREAM, 0 );
    servaddr.sun_family = AF_LOCAL;
    bcopy( ARP_PATH1, servaddr.sun_path, strlen( ARP_PATH1 ) );

    printf( "Issue an request to ARP for IP address: %s\n", IPAddr );
    Connect( sockFd, (SA *) &servaddr, sizeof( servaddr ) );
    Send( sockFd, IPAddr, IP_SIZE, 0);

    fd_set rset;
    FD_ZERO( &rset );
    FD_SET( sockFd, &rset );
    struct timeval timeOut;
    // Must set both fields (tv_sec and tv_usec). Leaving one of
    // the field uninitialized results in undesired behavior.
    timeOut.tv_sec = 5;
    timeOut.tv_usec = 0;
    const NpInt32 nReady = MySelect( sockFd+1, &rset, NULL, NULL, &timeOut );

    if( FD_ISSET( sockFd, &rset ) )
    {
        NpInt32 nRead = recv
                        (
                        sockFd,
                        (char *)hwAddr,
                        sizeof( HwAddr ),
                        0
                        );
        if( nRead < 0 )
        {
            printf("In areq: recv error. Continue...\n");
            success = FALSE;
        }

        char MacPresentationAddr[MAC_PRESENTATION_SIZE];
        ToMACPresentationFormat( MacPresentationAddr, hwAddr->sll_addr );
        printf( "HW address returned! \nRetrive MAC address %s for IP address %s.\n", MacPresentationAddr, IPAddr );
    }

    if( 0 == nReady )
    {
        close( sockFd );
        printf("Timed out in areq! Continue...\n");
        success = FALSE;
    }

    return success;
}

