#include "unp.h"
#include <linux/if_packet.h>
#include <linux/if_ether.h>
#include <linux/if_arp.h>
#include "ArpCache.h"
#include "ArpTasks.h"
#include "ArpPacket.h"
#include "NetworkInterface.h"
#include "CommonTasks.h"
#include "Config.h"
#include "NpType.h"

NpInt32 main()
{
    unlink( ARP_PATH1 );
    NeworkInterfacesTable netIfTable[MAX_INTERFACE_PER_NODE];
    const NpUint32 numOfInterfaces = InitNetworkInterfaceTable( &netIfTable[0]);

    ARPCache arpCache[MAX_ARP_ENTRY];
    NpUint32 numOfArpCacheEntry = 0;
    InitARPCache( arpCache );

    char canonicalIPAddr[IP_SIZE];
    bzero( canonicalIPAddr, IP_SIZE );
    char *IPStr = GetCanonicalIPAddr( &netIfTable[0], numOfInterfaces );
    bcopy( IPStr, canonicalIPAddr, strlen( IPStr ) );
    printf("My IP address: %s\n", canonicalIPAddr);

    NpInt32 sockFd = Socket( PF_PACKET, SOCK_RAW, htons( ETH_P_ARP ) );

    // Listening socket for communicating with the function areq
    // implemented in Tour module.
    NpInt32 listenFd;
    SetupListeningSocket( &listenFd );

    fd_set rset, allrset;
    FD_ZERO( &allrset );
    FD_SET( sockFd, &allrset );
    FD_SET( listenFd, &allrset );
    NpInt32 maxfd = max( sockFd, listenFd );
    NpInt32 connFd = 0;

    for( ; ; )
    {
        rset = allrset;
        MySelect( (maxfd+1), &rset, NULL, NULL, NULL );
        //printf("After selection: arp(%d), listen(%d), connfd(%d)\n", sockFd, listenFd, connFd);

        if( FD_ISSET( sockFd, &rset ) )
        {
            struct sockaddr_ll sa;
            socklen_t salen = sizeof( struct sockaddr_ll );
            char recvBuffer[MAXLINE];
            bzero( &recvBuffer, MAXLINE );
            Recvfrom
                (
                sockFd,
                recvBuffer,
                ETH_FRAME_LEN,
                0,
                (SA *)&sa,
                &salen
                );
            ARPPacket packet;
            NpUchar *data = recvBuffer + ETHER_FRAME_HEADER_SIZE;
            bcopy( data, &packet, (NpUint32)sizeof( ARPPacket ) );

            if( packet.id != MY_ARP_ID )
            {
                continue;
            }

            char macPresentationDestAddr[MAC_PRESENTATION_SIZE];
            bzero( &macPresentationDestAddr, MAC_PRESENTATION_SIZE );
            ToMACPresentationFormat( macPresentationDestAddr, recvBuffer );

            char macPresentationSrcAddr[MAC_PRESENTATION_SIZE];
            bzero( &macPresentationSrcAddr, MAC_PRESENTATION_SIZE );
            ToMACPresentationFormat( macPresentationSrcAddr, recvBuffer + MAC_SIZE );
            printf( "Received an Ethernet frame\n(src: %s, dest: %s, proto: arp(0x806))\n", macPresentationSrcAddr, macPresentationDestAddr );
            DebugArpPacket( &packet );

            if( ARP_REQUEST == packet.op )
            {
                //printf("receive request %s, %s\n", packet.destIPAddr, canonicalIPAddr );
                if( 0 == bcmp( packet.destIPAddr, canonicalIPAddr, strlen( canonicalIPAddr ) ) )
                {
                    // I am the destination node
                    ARPCache cacheEntry;
                    BuildARPCacheEntry
                        (
                        &cacheEntry,
                        packet.srcIPAddr,
                        packet.srcMacAddr,
                        sa.sll_ifindex,
                        HA_TYPE,
                        -1,
                        FALSE
                        );

                    NpUint32 updatedEntryIndex = 0;
                    AddOrUpdateArpCacheEntry
                        (
                        &updatedEntryIndex,
                        &arpCache[0],
                        &numOfArpCacheEntry,
                        &cacheEntry,
                        FALSE
                        );

                    char *pMacStr = GetMainMacAddr( &netIfTable[0], numOfInterfaces );
                    char newSrcIPAddr[IP_SIZE];
                    char newSrcMacAddr[MAC_SIZE];
                    char newDestIPAddr[IP_SIZE];
                    char newDestMacAddr[MAC_SIZE];
                    bzero( &newSrcIPAddr, IP_SIZE );
                    bzero( &newSrcMacAddr, MAC_SIZE );
                    bzero( &newDestIPAddr, IP_SIZE );
                    bzero( &newDestMacAddr, MAC_SIZE );
                    bcopy( packet.destIPAddr, newSrcIPAddr, IP_SIZE );
                    ToMACSuccinctFormat( newSrcMacAddr, pMacStr );
                    bcopy( packet.srcIPAddr, newDestIPAddr, IP_SIZE );
                    bcopy( packet.srcMacAddr, newDestMacAddr, MAC_SIZE );
                    BuildARPPacket
                        (
                        &packet,
                        ARP_REPLY,
                        newSrcIPAddr,
                        newSrcMacAddr,
                        newDestIPAddr,
                        newDestMacAddr
                        );

                    ReplyARPRequestMsg
                        (
                        sockFd,
                        &packet,
                        sa.sll_ifindex
                        );
                }
                else
                {
                    // Do nothing
                }
            }
            else if( ARP_REPLY == packet.op )
            {
                //printf( "Received a reply %s,%s\n", canonicalIPAddr, packet.destIPAddr );
                if( 0 != bcmp( canonicalIPAddr, packet.destIPAddr, strlen( canonicalIPAddr ) ) )
                {
                    err_sys( "This node should not receive arp reply\n" );
                }
                ARPCache cacheEntry;
                BuildARPCacheEntry
                    (
                    &cacheEntry,
                    packet.srcIPAddr,
                    packet.srcMacAddr,
                    sa.sll_ifindex,
                    HA_TYPE,
                    -1,
                    FALSE
                    );
                NpUint32 updatedIndex = 0;
                NpBool updated = AddOrUpdateArpCacheEntry
                                    (
                                    &updatedIndex,
                                    &arpCache[0],
                                    &numOfArpCacheEntry,
                                    &cacheEntry,
                                    FALSE
                                    );

                if( TRUE == updated )
                {
                    // We can use this entry!
                    HwAddr hwAddrReply;
                    BuildHwAddr
                        (
                        &hwAddrReply,
                        arpCache[updatedIndex].sll_ifindex,
                        arpCache[updatedIndex].sll_hatype,
                        sizeof(arpCache[updatedIndex].sll_hatype),
                        arpCache[updatedIndex].macAddr
                        );

                    //DebugHwAddr( &hwAddrReply );

                    send
                        (
                        connFd,
                        (void *) &hwAddrReply,
                        sizeof( HwAddr ),
                        0
                        );
                    maxfd = max( sockFd, listenFd );
                    close( connFd );
                }
                else
                {
                    err_sys( "You should have added or updated something" );
                }
            }
            else
            {
                err_sys( "Unknown arp packet type" );
            }
        }

        if( FD_ISSET( listenFd, &rset ) )
        {
            struct sockaddr_un cliaddr;
            socklen_t clilen = sizeof(cliaddr);
            connFd = Accept( listenFd, (SA *) &cliaddr, &clilen );
            FD_SET( connFd, &allrset );
            maxfd = max( maxfd, connFd );
            //printf( "Connection accepted: connFd(%d)\n", connFd );
        }

        if( FD_ISSET( connFd, &rset ) )
        {
            char recvBuf[MAXLINE];
            bzero( &recvBuf, MAXLINE );
            const nRead = Recv( connFd, recvBuf, MAXLINE, 0 );
            if( 0 == nRead )
            {
                printf( "Connection gracefully closed\n" );
                maxfd = max( sockFd, listenFd );
                close( connFd );
                continue;
            }
            if( 0 == bcmp( canonicalIPAddr, recvBuf, strlen( canonicalIPAddr ) ) )
            {
                NpUint32 foundIndex = 0;
                NpBool found = FindEntryInARPCache
                                    (
                                    &foundIndex,
                                    &arpCache[0],
                                    numOfArpCacheEntry,
                                    recvBuf
                                    );
                if( TRUE == found )
                {
                    // We can use this entry!
                    HwAddr hwAddrReply;
                    BuildHwAddr
                        (
                        &hwAddrReply,
                        arpCache[foundIndex].sll_ifindex,
                        arpCache[foundIndex].sll_hatype,
                        sizeof(arpCache[foundIndex].sll_hatype),
                        arpCache[foundIndex].macAddr
                        );

                    send
                        (
                        connFd,
                        (void *) &hwAddrReply,
                        sizeof( HwAddr ),
                        0
                        );

                    // Update the connFd in ArpCache
                    arpCache[foundIndex].connFd = connFd;
                    maxfd = max( sockFd, listenFd );
                    close( connFd );
                }
                else
                {
                    char *pMacStr = GetMainMacAddr( &netIfTable[0], numOfInterfaces );
                    const NpInt32 ifIndex = GetMainInterface( &netIfTable[0], numOfInterfaces );
                    char succintMacAddr[MAC_SIZE];
                    bzero( &succintMacAddr, MAC_SIZE );
                    ToMACSuccinctFormat( succintMacAddr, pMacStr );

                    ARPCache cacheEntry;
                    BuildARPCacheEntry
                        (
                        &cacheEntry,
                        canonicalIPAddr,
                        succintMacAddr,
                        ifIndex,
                        HA_TYPE,
                        connFd,
                        TRUE
                        );

                    NpUint32 updatedEntryIndex = 0;
                    AddOrUpdateArpCacheEntry
                        (
                        &updatedEntryIndex,
                        &arpCache[0],
                        &numOfArpCacheEntry,
                        &cacheEntry,
                        TRUE
                        );

                    HwAddr hwAddrReply;
                    BuildHwAddr
                        (
                        &hwAddrReply,
                        arpCache[updatedEntryIndex].sll_ifindex,
                        arpCache[updatedEntryIndex].sll_hatype,
                        sizeof(arpCache[updatedEntryIndex].sll_hatype),
                        arpCache[updatedEntryIndex].macAddr
                        );

                    send
                        (
                        connFd,
                        (void *) &hwAddrReply,
                        sizeof( HwAddr ),
                        0
                        );

                    // Update the connFd in ArpCache
                    arpCache[foundIndex].connFd = connFd;
                    maxfd = max( sockFd, listenFd );
                    close( connFd );
                }
            }
            else
            {
                NpUint32 foundIndex = 0;
                NpBool found = FindEntryInARPCache
                                    (
                                    &foundIndex,
                                    &arpCache[0],
                                    numOfArpCacheEntry,
                                    recvBuf
                                    );
                if( TRUE == found )
                {
                    // We can use this entry!
                    HwAddr hwAddrReply;
                    BuildHwAddr
                        (
                        &hwAddrReply,
                        arpCache[foundIndex].sll_ifindex,
                        arpCache[foundIndex].sll_hatype,
                        sizeof(arpCache[foundIndex].sll_hatype),
                        arpCache[foundIndex].macAddr
                        );

                    send
                        (
                        connFd,
                        (void *) &hwAddrReply,
                        sizeof(HwAddr),
                        0
                        );

                    // Update the connFd in ArpCache
                    arpCache[foundIndex].connFd = connFd;
                    maxfd = max( sockFd, listenFd );
                    close( connFd );
                }
                else
                {
                    // Broadcast message to acquire hw address
                    FloodARPRequestMsg
                        (
                        sockFd,
                        recvBuf,
                        &netIfTable[0],
                        numOfInterfaces
                        );
                }
            }
        }
    }
}


