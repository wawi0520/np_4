#ifndef _COMMON_TASKS_H_
#define _COMMON_TASKS_H_

#include "NetworkInterface.h"

typedef struct hwaddr
{
    NpInt32 sll_ifindex;        /* Interface number */
    unsigned short sll_hatype;  /* Hardware type */
    NpUchar sll_halen;          /* Length of address */
    NpUchar sll_addr[8];        /* Physical layer address */
} HwAddr;

void BuildHwAddr
    (
    HwAddr *hwAddr,
    NpInt32 sll_ifindex,
    unsigned short sll_hatype,
    NpUchar sll_halen,
    NpUchar *sll_addr
    );

void DebugHwAddr( HwAddr *hwAddr );

char *GetCanonicalIPAddr( NeworkInterfacesTable *table, const NpUint32 count );

char *GetMainMacAddr( NeworkInterfacesTable *table, const NpUint32 count );

const NpInt32 GetMainInterface( NeworkInterfacesTable *table, const NpUint32 count );

NpInt32 MySelect
    (
    NpInt32 nfds,
    fd_set *readfds,
    fd_set *writefds,
    fd_set *exceptfds,
    struct timeval *timeout
    );

void IPAddrToHostName( char *hostname, char *IPAddr );

void SetupListeningSocket( NpInt32 *listenFd );

#endif
