#include "ArpPacket.h"
#include "unp.h"

//=============================================================================
void InitARPPacket( ARPPacket *packet )
{
    packet->id = MY_ARP_ID;
    packet->hardType = 1;
    packet->protType = 0x800;
    packet->hardSize = sizeof( packet->hardType );
    packet->protSize = sizeof( packet->protSize );
    packet->op = ARP_REQUEST;
    bzero( &packet->srcIPAddr, IP_SIZE );
    bzero( &packet->srcMacAddr, MAC_SIZE );
    bzero( &packet->destIPAddr, IP_SIZE );
    bzero( &packet->destMacAddr, MAC_SIZE );
}

//=============================================================================
void BuildARPPacket
    (
    ARPPacket *packet,
    const NpUint16 op,
    char *srcIPAddr,
    char *srcMacAddr,
    char *destIPAddr,
    char *destMacAddr
    )
{
    packet->id = MY_ARP_ID;
    packet->hardType = 1;
    packet->protType = 0x800;
    packet->hardSize = sizeof( packet->hardType );
    packet->protSize = sizeof( packet->protSize );
    packet->op = op;
    if( NULL != srcIPAddr )
    {
        bcopy( srcIPAddr, packet->srcIPAddr, IP_SIZE );
    }
    if( NULL != srcMacAddr )
    {
        bcopy( srcMacAddr, packet->srcMacAddr, MAC_SIZE );
    }
    if( NULL != destIPAddr )
    {
        bcopy( destIPAddr, packet->destIPAddr, IP_SIZE );
    }
    if( NULL != destMacAddr )
    {
        bcopy( destMacAddr, packet->destMacAddr, MAC_SIZE );
    }
}

//=============================================================================
void DebugArpPacket( ARPPacket *packet )
{
    char presentationMacAddr[MAC_PRESENTATION_SIZE];

    printf("==================ARP Packet===================\n");
    printf( "id: %u\n", packet->id );
    printf( "hardType: %u\n", packet->hardType );
    printf( "protType: %#04x\n", packet->protType );
    printf( "hardSize: %u\n", packet->hardSize );
    printf( "protSize: %u\n", packet->protSize );
    printf( "op: %s\n", (ARP_REQUEST == packet->op)? "request" : "reply" );
    printf( "srcIPAddr: %s\n", packet->srcIPAddr );

    bzero( &presentationMacAddr, MAC_PRESENTATION_SIZE );
    ToMACPresentationFormat( presentationMacAddr, packet->srcMacAddr );
    printf( "srcMacAddr: %s\n", presentationMacAddr );

    printf( "destIPAddr: %s\n", packet->destIPAddr );

    bzero( &presentationMacAddr, MAC_PRESENTATION_SIZE );
    ToMACPresentationFormat( presentationMacAddr, packet->destMacAddr );
    printf( "destMacAddr: %s\n", presentationMacAddr );
    printf( "==============================================\n" );
}

