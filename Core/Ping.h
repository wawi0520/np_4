#ifndef _PING_H_
#define _PING_H_

#include "unp.h"
#include <netinet/in_systm.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include "CommonTasks.h"
#include "NpType.h"

const NpBool PingNode
    (
    const NpInt32 fd,
    char *srcIPAddr,
    char *srcMacAddr,
    const NpInt32 ifIndex,
    char *IPAddr,
    HwAddr *hwAddr
    );

#endif
