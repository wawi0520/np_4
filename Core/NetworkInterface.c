/**
    This implements the struct and functions related to the
    network interface.
    @file    NetworkInterface.c
    @author  Wei-Ying Tsai
    @contact wetsai@cs.stonybrook.edu
    @date 2015.11.22
*/
#include "NetworkInterface.h"
#include <stdio.h>
#include "unp.h"
#include "hw_addrs.h"

//=============================================================================
const NpUint32 InitNetworkInterfaceTable( NeworkInterfacesTable *table )
{
    // This function is adapted from the main function
    // defined in Prhwaddrs.c

    struct hwa_info *hwa = Get_hw_addrs(); // Get first entry
    struct hwa_info *head = hwa;
    struct sockaddr *sa;
    char   *ptr;
    int    i, prflag,ifindex;
    NpUint32 count = 0;

    for( ; NULL != hwa; hwa = hwa->hwa_next, ++count )
    {
        // Clear the memory first
        bzero( &table[count].name, IF_NAME_SIZE);
        bzero( &table[count].IPAddr, IP_SIZE);
        bzero( &table[count].hwAddr, MAC_PRESENTATION_SIZE );

        bcopy( hwa->if_name, table[count].name, strlen( hwa->if_name ) );

        if ( NULL != ( sa = hwa->ip_addr ) )
        {
            strcpy( table[count].IPAddr, (char *)Sock_ntop_host(sa, sizeof(*sa)));
        }

        prflag = 0;
        i = 0;
        do {
            if (hwa->if_haddr[i] != '\0') {
                prflag = 1;
                break;
            }
        } while (++i < IF_HADDR);

        if (prflag) {
            ptr = hwa->if_haddr;
            i = IF_HADDR;
            do {
                char segment[4];
                bzero( &segment, 4 );

                sprintf( segment,"%.2x%s", *ptr++ & 0xff, (i == 1) ? "" : ":");
                const catLength = ( i == 1 )? 2 : 3;
                strncat( table[count].hwAddr, segment, catLength );
            } while (--i > 0);
        }

        table[count].interfaceIndex = hwa->if_index;
    }

    free_hwa_info( head );

    return count;
}

//=============================================================================
void DebugNetworkInterfaceTable( NeworkInterfacesTable *table, const NpUint32 count )
{
    printf("===============Network Interfaces===============\n");
    NpUint32 i = 0;
    for( ; i < count; ++i )
    {
        printf( "Interface: %d\n", i + 1 );
        printf( "\t%d\n", table[i].interfaceIndex );
        printf( "\t%s(%u)\n", table[i].name, (NpInt32)strlen(table[i].name) );
        printf( "\t%s(%u)\n", table[i].IPAddr, (NpInt32)strlen(table[i].IPAddr) );
        printf( "\t%s(%u)\n", table[i].hwAddr, (NpInt32)strlen(table[i].hwAddr) );
    }
    printf( "===============================================\n" );
}

//=============================================================================
void ToMACPresentationFormat( char *presentation, char *succinct )
{
    bzero( presentation, MAC_PRESENTATION_SIZE );

    char *ptr = succinct;
    NpUint32 i = MAC_SIZE;
    do {
        char segment[4];
        bzero( &segment, 4 );

        sprintf( segment,"%.2x%s", *ptr++ & 0xff, (i == 1) ? "" : ":");
        const catLength = ( i == 1 )? 2 : 3;
        strncat( presentation, segment, catLength );
    } while (--i > 0);

    //printf( "MAC Presentation Addr: %s\n", presentation );
}

//=============================================================================
void ToMACSuccinctFormat( char *succinct, char *presentation )
{
    sscanf( presentation, "%hhx:%hhx:%hhx:%hhx:%hhx:%hhx", &succinct[0], &succinct[1], &succinct[2], &succinct[3], &succinct[4], &succinct[5] );
    //printf("In ToMACSuccinctFormat: %hhx:%hhx:%hhx:%hhx:%hhx\n", succinct[0], succinct[1], succinct[2], succinct[3], succinct[4], succinct[5] );
}


