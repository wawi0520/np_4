
#ifndef _ARP_CACHE_H_
#define _ARP_CACHE_H_
#include "Config.h"
#include "NpType.h"

typedef struct
{
    NpBool valid;
    char IPAddr[IP_SIZE];
    char macAddr[MAC_SIZE];
    NpInt32 sll_ifindex;
    NpInt32 sll_hatype;
    NpInt32 connFd;
} ARPCache;

void InitARPCache( ARPCache *arpCache );

void BuildARPCacheEntry
    (
    ARPCache *entry,
    char *IPAddr,
    char *macAddr,
    const NpInt32 sll_ifindex,
    const NpInt32 sll_hatype,
    const NpInt32 connFd,
    const NpBool updateConnFd
    );

NpBool FindEntryInARPCache
    (
    NpUint32 *foundIndex,
    ARPCache *arpCache,
    const NpUint32 count,
    char *IPAddr
    );

NpBool AddOrUpdateArpCacheEntry
    (
    NpUint32 *updatedEntryIndex,
    ARPCache *arpCache,
    NpUint32 *count,
    ARPCache *cacheEntry,
    const NpBool updateConnFd
    );


#endif

