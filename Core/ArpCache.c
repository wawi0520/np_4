#include "ArpCache.h"

//=============================================================================
void InitARPCache( ARPCache *arpCache )
{
    NpUint32 i = 0;
    for( ; i < MAX_ARP_ENTRY; ++i )
    {
        arpCache[i].valid = FALSE;
        bzero( &arpCache[i].IPAddr, IP_SIZE );
        bzero( &arpCache[i].macAddr, MAC_SIZE );
        arpCache[i].sll_ifindex = 0;
        arpCache[i].sll_hatype = 0;
        arpCache[i].connFd = 0;
    }
}

//=============================================================================
void BuildARPCacheEntry
    (
    ARPCache *entry,
    char *IPAddr,
    char *macAddr,
    const NpInt32 sll_ifindex,
    const NpInt32 sll_hatype,
    const NpInt32 connFd,
    const NpBool updateConnFd
    )
{
    bzero( entry, sizeof( ARPCache ) );
    bcopy( IPAddr, entry->IPAddr, IP_SIZE );
    bcopy( macAddr, entry->macAddr, MAC_SIZE );
    entry->sll_ifindex = sll_ifindex;
    entry->sll_hatype = sll_hatype;
    if( TRUE == updateConnFd )
    {
        entry->connFd = connFd;
    }
}

//=============================================================================
NpBool FindEntryInARPCache
    (
    NpUint32 *foundIndex,
    ARPCache *arpCache,
    const NpUint32 count,
    char *IPAddr
    )
{
    NpUint32 i = 0;
    for( ; i < count; ++i )
    {
        if( 0 == bcmp( IPAddr, arpCache[i].IPAddr, strlen( IPAddr )) )
        {
            *foundIndex = i;
            return TRUE;
        }
    }

    return FALSE;
}

//=============================================================================
NpBool AddOrUpdateArpCacheEntry
    (
    NpUint32 *updatedEntryIndex,
    ARPCache *arpCache,
    NpUint32 *count,
    ARPCache *cacheEntry,
    const NpBool updateConnFd
    )
{
    NpUint32 foundIndex = 0;
    NpBool entryFound = FindEntryInARPCache
                            (
                            &foundIndex,
                            arpCache,
                            *count,
                            cacheEntry->IPAddr
                            );

    if( entryFound )
    {
        *updatedEntryIndex = foundIndex;

        // No need to update IP address
        bcopy( cacheEntry->macAddr, arpCache[foundIndex].macAddr, MAC_SIZE );
        arpCache[foundIndex].sll_ifindex= cacheEntry->sll_ifindex;
        arpCache[foundIndex].sll_hatype= cacheEntry->sll_hatype;
        if( TRUE == updateConnFd )
        {
            arpCache[foundIndex].connFd = cacheEntry->connFd;
        }
        return TRUE;
    }
    else
    {
        if( *count == MAX_ARP_ENTRY )
        {
            err_sys( "Maxmim #of arp entry reached. Try to increase the size\n" );
        }
        const NpUint32 curIndex = *count;
        //printf("curIndex:%d (%d)\n",curIndex, cacheEntry->sll_ifindex);
        BuildARPCacheEntry
            (
            &arpCache[curIndex],
            cacheEntry->IPAddr,
            cacheEntry->macAddr,
            cacheEntry->sll_ifindex,
            cacheEntry->sll_hatype,
            cacheEntry->connFd,
            updateConnFd
            );

        *updatedEntryIndex = curIndex;
        *count = *count + 1;
        return TRUE;
    }
}

