
#ifndef _TOUR_TASKS_H_
#define _TOUR_TASKS_H_
#include "unp.h"
#include "TourPacket.h"
#include "CommonTasks.h"
#include "Config.h"
#include "NpType.h"

void SetupRtSocket( NpInt32 *fd );

void SendTourPacket( const NpInt32 fd, TourPacket *packet );

NpBool areq
    (
    struct sockaddr *sa,
    socklen_t sockaddrlen,
    HwAddr *hwAddr
    );

#endif
