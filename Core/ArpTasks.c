#include "ArpTasks.h"
#include <linux/if_packet.h>
#include <linux/if_ether.h>
#include <linux/if_arp.h>
#include "unp.h"
#include "ArpPacket.h"
#include "NetworkInterface.h"
#include "Config.h"
#include "NpType.h"

//=============================================================================
NpBool FloodARPRequestMsg
    (
    const NpInt32 fd,
    char *IPAddr,
    NeworkInterfacesTable *ifTable,
    const NpUint32 numOfInterfaces
    )
{
    struct sockaddr_ll socket_address;
    void* buffer = (void*)calloc(ETH_FRAME_LEN,1);
    unsigned char* etherhead = buffer;
    unsigned char* data = buffer + ETHER_FRAME_HEADER_SIZE;

    struct ethhdr *eh = (struct ethhdr *)etherhead;

    /*other host MAC address*/
    NpUchar dest_mac[MAC_SIZE];
    // The host MAC address is the broadcast address
    memset( dest_mac, 0xFF, MAC_SIZE );

    socket_address.sll_family = PF_PACKET;
    socket_address.sll_protocol = htons( ETH_P_IP );
    socket_address.sll_hatype = ARPHRD_ETHER;
    socket_address.sll_pkttype = PACKET_OTHERHOST;
    socket_address.sll_halen = ETH_ALEN;

    NpUint32 i = 0;
    for( ; i < numOfInterfaces; ++i )
    {
        if( 0 == bcmp( ifTable[i].name, "eth0", strlen("eth0") ) )
        {
            socket_address.sll_ifindex = ifTable[i].interfaceIndex;
            ToMACSuccinctFormat( socket_address.sll_addr, ifTable[i].hwAddr );
            socket_address.sll_addr[6] = 0x00;
            socket_address.sll_addr[7] = 0x00;

            /*set the frame header*/
            memcpy((void*)buffer, (void*)dest_mac, ETH_ALEN);
            memcpy((void*)(buffer+ETH_ALEN), (void*)socket_address.sll_addr, ETH_ALEN);
            eh->h_proto = htons( ETH_P_ARP );

            /*set up frame's payload*/
            ARPPacket arpPacket;
            InitARPPacket( &arpPacket );
            BuildARPPacket
                (
                &arpPacket,
                ARP_REQUEST,
                ifTable[i].IPAddr,
                socket_address.sll_addr,
                IPAddr,
                NULL
                );

            bcopy( &arpPacket, data, (NpUint32)sizeof( arpPacket ) );

            NpInt32 send_result = sendto
                                (
                                fd,
                                buffer,
                                ETH_FRAME_LEN,
                                0,
                                (SA *) &socket_address,
                                sizeof(socket_address)
                                );

            if ( -1 == send_result )
            {
                // Send error, may be because the destination ARP
                // is not running. Continue...
                printf( "sendto error: %s. Continue...\n", strerror(errno) );
                return FALSE;
            }

            char thisHostNode[MAX_LINE];
            char toHostNode[MAX_LINE];
            IPAddrToHostName( thisHostNode, ifTable[i].IPAddr );
            IPAddrToHostName( toHostNode, arpPacket.destIPAddr );

            printf( "%s broadcasts ARP request to ask %s\n", thisHostNode, toHostNode );

            char macPresentationDestAddr[MAC_PRESENTATION_SIZE];
            bzero( &macPresentationDestAddr, MAC_PRESENTATION_SIZE );
            ToMACPresentationFormat( macPresentationDestAddr, dest_mac );

            char macPresentationSrcAddr[MAC_PRESENTATION_SIZE];
            bzero( &macPresentationSrcAddr, MAC_PRESENTATION_SIZE );
            ToMACPresentationFormat( macPresentationSrcAddr, socket_address.sll_addr );
            printf( "Sending Ethernet frame\n(src: %s, dest: %s, proto: arp(%#04x))\n", macPresentationSrcAddr, macPresentationDestAddr, ntohs(eh->h_proto) );

            DebugArpPacket( &arpPacket );
            break;
        }


    }

    return TRUE;
}

//=============================================================================
NpBool ReplyARPRequestMsg
    (
    const NpInt32 fd,
    ARPPacket *packet,
    NpInt32 sll_ifindex
    )
{
    struct sockaddr_ll socket_address;
    void* buffer = (void*)calloc(ETH_FRAME_LEN,1);
    unsigned char* etherhead = buffer;
    unsigned char* data = buffer + ETHER_FRAME_HEADER_SIZE;

    struct ethhdr *eh = (struct ethhdr *)etherhead;

    bcopy( packet->srcMacAddr, socket_address.sll_addr, MAC_SIZE );
    socket_address.sll_addr[6] = 0x00;
    socket_address.sll_addr[7] = 0x00;

    /*other host MAC address*/
    NpUchar dest_mac[MAC_SIZE];
    bcopy( packet->destMacAddr, dest_mac, MAC_SIZE );

    socket_address.sll_family = PF_PACKET;
    socket_address.sll_protocol = htons( ETH_P_IP );
    socket_address.sll_hatype = ARPHRD_ETHER;
    socket_address.sll_pkttype = PACKET_OTHERHOST;
    socket_address.sll_halen = ETH_ALEN;
    socket_address.sll_ifindex = sll_ifindex;

    /*set the frame header*/
    memcpy((void*)buffer, (void*)dest_mac, ETH_ALEN);
    memcpy((void*)(buffer+ETH_ALEN), (void*)socket_address.sll_addr, ETH_ALEN);
    eh->h_proto = htons( ETH_P_ARP );

    bcopy( packet, data, (NpUint32)sizeof( ARPPacket ) );

    NpInt32 send_result = sendto
                            (
                            fd,
                            buffer,
                            ETH_FRAME_LEN,
                            0,
                            (SA *) &socket_address,
                            sizeof(socket_address)
                            );

    if ( -1 == send_result )
    {
        // Send error, may be because the destination ARP
        // is not running. Continue...
        printf( "sendto error: %s. Continue...\n", strerror(errno) );
        return FALSE;
    }

    char thisHostNode[MAX_LINE];
    char toHostNode[MAX_LINE];
    IPAddrToHostName( thisHostNode, packet->srcIPAddr );
    IPAddrToHostName( toHostNode, packet->destIPAddr );

    printf( "%s replies ARP request to %s\n", thisHostNode, toHostNode );

    char macPresentationDestAddr[MAC_PRESENTATION_SIZE];
    bzero( &macPresentationDestAddr, MAC_PRESENTATION_SIZE );
    ToMACPresentationFormat( macPresentationDestAddr, dest_mac );

    char macPresentationSrcAddr[MAC_PRESENTATION_SIZE];
    bzero( &macPresentationSrcAddr, MAC_PRESENTATION_SIZE );
    ToMACPresentationFormat( macPresentationSrcAddr, socket_address.sll_addr );
    printf( "Sending Ethernet frame\n(src: %s, dest: %s, proto: arp(%#04x))\n", macPresentationSrcAddr, macPresentationDestAddr, ntohs(eh->h_proto) );
    DebugArpPacket( packet );
    return TRUE;
}

