#ifndef _ARP_PACKET_H_
#define _ARP_PACKET_H_
#include "Config.h"
#include "NpType.h"

#define ARP_REPLY 0
#define ARP_REQUEST 1

typedef struct
{
    NpUint16 id;
    NpUint16 hardType;
    NpUint16 protType;
    NpUint8 hardSize;
    NpUint8 protSize;
    NpUint16 op;
    char srcIPAddr[IP_SIZE];
    char srcMacAddr[MAC_SIZE];
    char destIPAddr[IP_SIZE];
    char destMacAddr[MAC_SIZE];
} ARPPacket;

void InitARPPacket( ARPPacket *packet );
void BuildARPPacket
    (
    ARPPacket *packet,
    const NpUint16 type,
    char *srcIPAddr,
    char *srcMacAddr,
    char *destIPAddr,
    char *destMacAddr
    );

void DebugArpPacket( ARPPacket *packet );

#endif
