/**
    This declares serveral useful config (macro)
    @file    Config.h
    @author  Wei-Ying Tsai
    @contact wetsai@cs.stonybrook.edu
    @date 2015.11.22
*/

#ifndef _CONFIG_H_
#define _CONFIG_H_

#include "NpType.h"

#define MAX_LINE 128
#define SMALL_MSG_SIZE 64
#define IP_SIZE 32
// The MAC 'presentation' address will look like xx:xx:xx:xx:xx:xx
#define MAC_PRESENTATION_SIZE 32
// The MAC address will contain only hexadecimal value (no ':')
#define MAC_SIZE 6
#define IF_NAME_SIZE 32
#define ETHER_FRAME_HEADER_SIZE 14

#define MY_ARP_PROTOCOL 51414
#define MY_PING_PROTOCOL 51415
#define ARP_PATH1 "/tmp/ARP_wetsai_51414"
#define SUN_PATH_SIZE 128

// Multicast
#define MULTICAST_IP_ADDR "224.0.1.1"
#define MULTICAST_PORT "51414"

// Network Interface
#define MAX_INTERFACE_PER_NODE 16

// Tour Packet
#define MAX_TOUR_NODE 32

#define RT_PROTOCOL 141 // This needs to be smaller than 256
#define PING_PROTOCOL 51414
#define IDENTIFICATION 605
#define TTL 1
#define MAX_PING_SENT 6

// ARP cache and packet
#define MAX_ARP_ENTRY 512
#define HA_TYPE 1
#define MY_ARP_ID 520

#define IPv4_HEADER_LENGTH 20
#define ICMP_HEADER_LENGTH 8
#define IP_DEFAULT_TTL 64

#endif
