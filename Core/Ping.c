#include "Ping.h"
#include <sys/socket.h>
#include <linux/if_packet.h>
#include <linux/if_ether.h>
#include <linux/if_arp.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include <time.h>
#include "unp.h"

//=============================================================================
const NpBool PingNode
    (
    const NpInt32 fd,
    char *srcIPAddr,
    char *srcMacAddr,
    const NpInt32 ifIndex,
    char *destIPAddr,
    HwAddr *hwAddr
    )
{
    static NpUint32 seqNum = 0;
    struct icmp icmp;
    NpInt32 len = 0;
    char icmpData[SMALL_MSG_SIZE];
    bzero( &icmpData, SMALL_MSG_SIZE );
    //memset( icmpData, 0xa5, SMALL_MSG_SIZE );
    bcopy( "wetsai", icmpData, strlen( "wetsai" ) );

    icmp.icmp_type = ICMP_ECHO;
    icmp.icmp_code = 0;
    icmp.icmp_id = ( getpid() & 0xffff );
    icmp.icmp_seq = seqNum;
    ++seqNum;
    Gettimeofday( (struct timeval *) icmp.icmp_data, NULL );

    len = 8 + SMALL_MSG_SIZE;
    icmp.icmp_cksum = 0;
    icmp.icmp_cksum = in_cksum( (u_short *) &icmp, len );

    struct ip ipHeader;
    Inet_pton( AF_INET, srcIPAddr, &ipHeader.ip_src );
    Inet_pton( AF_INET, destIPAddr, &ipHeader.ip_dst );
    ipHeader.ip_hl = IPv4_HEADER_LENGTH >> 2;
    ipHeader.ip_v = 4;
    ipHeader.ip_tos = 0;
    ipHeader.ip_len = htons( IPv4_HEADER_LENGTH + ICMP_HEADER_LENGTH + SMALL_MSG_SIZE );
    ipHeader.ip_id = htons( 0 );
    ipHeader.ip_off = htons( 0 );
    ipHeader.ip_ttl = IP_DEFAULT_TTL;
    ipHeader.ip_p = htons(IPPROTO_ICMP);
    ipHeader.ip_sum = 0;
    ipHeader.ip_sum = in_cksum((uint16_t *) &ipHeader, IPv4_HEADER_LENGTH );

    struct sockaddr_ll socket_address;
    bzero( &socket_address, sizeof( struct sockaddr_ll ) );
    void* buffer = (void*)calloc(ETH_FRAME_LEN,1);
    unsigned char* etherhead = buffer;
    unsigned char* data = buffer + ETHER_FRAME_HEADER_SIZE;

    struct ethhdr *eh = (struct ethhdr *)etherhead;

    bcopy( srcMacAddr, socket_address.sll_addr, MAC_SIZE );
    socket_address.sll_addr[6] = 0x00;
    socket_address.sll_addr[7] = 0x00;

    NpUchar dest_mac[MAC_SIZE];
    bcopy( hwAddr->sll_addr, dest_mac, MAC_SIZE );

    socket_address.sll_family = PF_PACKET;
    socket_address.sll_protocol = htons( ETH_P_IP );
    socket_address.sll_hatype = ARPHRD_ETHER;
    socket_address.sll_pkttype = PACKET_OTHERHOST;
    socket_address.sll_halen = ETH_ALEN;
    socket_address.sll_ifindex = ifIndex;

    memcpy((void*)buffer, (void*)dest_mac, ETH_ALEN);
    memcpy((void*)(buffer+ETH_ALEN), (void*)socket_address.sll_addr, ETH_ALEN);
    eh->h_proto = htons( ETH_P_IP );

    bcopy( &ipHeader, data, IPv4_HEADER_LENGTH );
    bcopy( &icmp, data + IPv4_HEADER_LENGTH, ICMP_HEADER_LENGTH );
    bcopy( &icmpData, data + IPv4_HEADER_LENGTH + ICMP_HEADER_LENGTH, SMALL_MSG_SIZE );

    //printf( "Start to ping %s\n", destIPAddr );
    NpInt32 send_result = sendto
                            (
                            fd,
                            buffer,
                            ETH_FRAME_LEN,
                            0,
                            (SA *) &socket_address,
                            sizeof(socket_address)
                            );

    if ( -1 == send_result )
    {
        // Send error, may be because the destination ARP
        // is not running. Continue...
        printf( "sendto error: %s. Continue...\n", strerror(errno) );
        return FALSE;
    }

    char toHostNode[MAX_LINE];
    IPAddrToHostName
        (
        toHostNode,
        destIPAddr
        );

    char testStr[MAX_LINE];
    bzero( &testStr, MAX_LINE );
    bcopy( data + IPv4_HEADER_LENGTH + ICMP_HEADER_LENGTH, testStr, SMALL_MSG_SIZE );
    printf( "PING %s (%s): %d data bytes (%s)\n", destIPAddr, toHostNode, (NpInt32)strlen(testStr), testStr );
}

//=============================================================================
u_short
in_cksum(addr, len)
    u_short *addr;
    int len;
{
    register int nleft = len;
    register u_short *w = addr;
    register int sum = 0;
    u_short answer = 0;

    /*
     * Our algorithm is simple, using a 32 bit accumulator (sum), we add
     * sequential 16 bit words to it, and at the end, fold back all the
     * carry bits from the top 16 bits into the lower 16 bits.
     */
    while (nleft > 1)  {
        sum += *w++;
        nleft -= 2;
    }

    /* mop up an odd byte, if necessary */
    if (nleft == 1) {
        *(u_char *)(&answer) = *(u_char *)w ;
        sum += answer;
    }

    /* add back carry outs from top 16 bits to low 16 bits */
    sum = (sum >> 16) + (sum & 0xffff);    /* add hi 16 to low 16 */
    sum += (sum >> 16);            /* add carry */
    answer = ~sum;                /* truncate to 16 bits */
    return (answer);
}

