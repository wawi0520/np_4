#include <linux/if_ether.h>
#include <linux/if_packet.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include "TourPacket.h"
#include "TourTasks.h"
#include "CommonTasks.h"
#include "NetworkInterface.h"
#include "Config.h"
#include "NpType.h"

NpInt32 main( int argc, char **argv )
{
    char canonicalIPAddr[IP_SIZE];
    bzero( canonicalIPAddr, IP_SIZE );
    NpInt32 rt = 0;
    NpInt32 pg = 0;
    NpInt32 multicastSendFd = 0;
    NpInt32 multicastRecvFd = 0;
    struct sockaddr *sasend;
    struct sockaddr *sarecv;
    socklen_t salen = 0;
    const NpInt32 on = 1;
    NpBool hasJoined = FALSE;
    NpBool StartToPingging = FALSE;
    NpBool StopPingging = FALSE;
    NpBool lastNodeReached = FALSE;
    NpUint32 pingSentCountSinceLastNodeReached = 0;

    NeworkInterfacesTable netIfTable[MAX_INTERFACE_PER_NODE];
    const NpUint32 numOfInterfaces = InitNetworkInterfaceTable( &netIfTable[0]);

    TourPacket tourPacket;
    InitTourPacket( &tourPacket );

    // Setup several sockets
    SetupRtSocket( &rt );
    pg = Socket( PF_PACKET, SOCK_RAW, htons( ETH_P_IP ) );
    //setsockopt( pg, IPPROTO_IP, IP_HDRINCL, &on, sizeof( NpInt32 ) );

    char *IPStr = GetCanonicalIPAddr( &netIfTable[0], numOfInterfaces );
    bcopy( IPStr, canonicalIPAddr, strlen( IPStr ) );
    printf( "IP address: %s(%d)\n", canonicalIPAddr, (NpInt32)strlen( canonicalIPAddr ) );

    if( argc > 1 )
    {
        BuildTourPacket
        (
        &tourPacket,
        canonicalIPAddr,
        &argv[0],
        argc - 1,
        MULTICAST_IP_ADDR,
        (NpPort)(atoi(MULTICAST_PORT))
        );
        DebugTourPacket( &tourPacket );

        multicastSendFd = Udp_client
                            (
                            MULTICAST_IP_ADDR,
                            MULTICAST_PORT,
                            (SA **) &sasend,
                            &salen
                            );
        multicastRecvFd = Socket( sasend->sa_family, SOCK_DGRAM, 0 );
        Setsockopt( multicastRecvFd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof( on ) );
        sarecv = Malloc(salen);
        bcopy( sasend, sarecv, salen );
        Bind( multicastRecvFd, sarecv, salen );

        Mcast_join( multicastRecvFd, sasend, salen, NULL, 0 );
        Mcast_set_loop( multicastSendFd, 1 ); //Need to loopback
        hasJoined = TRUE;

        SendTourPacket( rt, &tourPacket );
    }

    /*printf( "argc = %d\n", argc );
    printf( "rt = %d\n", rt );
    printf( "pg = %d\n", pg );*/

    fd_set rset, allrset;

    char pingIPAddr[IP_SIZE];
    bzero( &pingIPAddr, IP_SIZE );
    NpBool idReplySent = FALSE;
    for( ; ; )
    {
        FD_ZERO( &allrset );
        FD_SET( rt, &allrset );
        FD_SET( pg, &allrset );
        FD_SET( multicastRecvFd, &allrset );
        NpInt32 maxfd = max( rt, pg );
        maxfd = max( maxfd, multicastRecvFd );
        NpInt32 nReady = -1;
        rset = allrset;

        // Must set both fields (tv_sec and tv_usec). Leaving one of
        // the field uninitialized results in undesired behavior.
        struct timeval timeOut;
        if( MAX_PING_SENT == pingSentCountSinceLastNodeReached || TRUE == StopPingging )
        {
            if( TRUE == idReplySent )
            {
                timeOut.tv_sec = 5;
                timeOut.tv_usec = 0;
                nReady = MySelect( (maxfd+1), &rset, NULL, NULL, &timeOut );
            }
            else
            {
                nReady = MySelect( (maxfd+1), &rset, NULL, NULL, NULL );
            }
        }
        else
        {
            timeOut.tv_sec = 1;
            timeOut.tv_usec = 0;
            nReady = MySelect( (maxfd+1), &rset, NULL, NULL, &timeOut );
        }

        if( FD_ISSET( rt, &rset ) )
        {
            NpUchar buffer[MAXLINE];
            bzero( &buffer, MAXLINE );
            struct sockaddr_in sa;
            socklen_t len = sizeof( sa );

            Recvfrom( rt, buffer, MAXLINE, 0, (SA*) &sa, &len);
            struct iphdr *ipHeader = (struct iphdr *)buffer;
            if( ipHeader->id != htons( IDENTIFICATION ) )
            {
                // Not my packet...
                continue;
            }
            TourPacket *recvTourPacket = (TourPacket *)( buffer + sizeof(struct iphdr) );
            DebugTourPacket( recvTourPacket );

            time_t ticks= time(NULL);
            char fromHostNode[MAX_LINE];
            IPAddrToHostName
                (
                fromHostNode,
                recvTourPacket->nodeList[(recvTourPacket->indexToCurrentNode)-1]
                );
            printf( "<%.24s> received source routing packet from <%s>\n", ctime(&ticks), fromHostNode );

            const NpBool needToSendTourPacket =
                ( '\0' == recvTourPacket->nodeList[recvTourPacket->indexToCurrentNode+1][0] )?
                FALSE : TRUE;
            if( needToSendTourPacket )
            {
                SendTourPacket( rt, recvTourPacket );
            }
            else
            {
                lastNodeReached = TRUE;
            }

            if( FALSE == hasJoined )
            {
                // If this node does not join the multicast, it means
                // this node is not the source node. We need to join now.
                char portBuffer[MAXLINE];
                bzero( &portBuffer, MAXLINE );
                sprintf( portBuffer, "%u", (NpUint32)recvTourPacket->multicastPort );
                multicastSendFd = Udp_client
                                    (
                                    recvTourPacket->multicastIPAddr,
                                    portBuffer,
                                    (SA **) &sasend,
                                    &salen
                                    );
                multicastRecvFd = Socket( sasend->sa_family, SOCK_DGRAM, 0 );
                Setsockopt( multicastRecvFd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof( on ) );
                sarecv = Malloc(salen);
                bcopy( sasend, sarecv, salen );
                Bind( multicastRecvFd, sarecv, salen );

                Mcast_join( multicastRecvFd, sasend, salen, NULL, 0 );
                Mcast_set_loop( multicastSendFd, 1 ); //Need to loopback
                hasJoined = TRUE;
            }

            if( FALSE == StartToPingging )
            {
                //FindPreviousNode( pingIPAddr, &recvTourPacket->nodeList, canonicalIPAddr );
                if( TRUE == needToSendTourPacket )
                {
                    bcopy( recvTourPacket->nodeList[recvTourPacket->indexToCurrentNode-2], pingIPAddr, IP_SIZE );
                }
                else
                {
                    bcopy( recvTourPacket->nodeList[recvTourPacket->indexToCurrentNode-1], pingIPAddr, IP_SIZE );
                }
                StartToPingging = TRUE;
                printf( "IP address: %s\n", pingIPAddr );
            }
        }

        if( FD_ISSET( pg, &rset ) )
        {
            struct sockaddr_ll sa;
            socklen_t salen = sizeof( struct sockaddr_ll );
            char recvBuffer[MAXLINE];
            bzero( &recvBuffer, MAXLINE );
            Recvfrom
                (
                pg,
                recvBuffer,
                ETH_FRAME_LEN,
                0,
                (SA *)&sa,
                &salen
                );
            struct ip ipPacket;
            struct icmp icmpPacket;
            char icmpData[SMALL_MSG_SIZE];
            NpUchar *data = recvBuffer + ETHER_FRAME_HEADER_SIZE;
            bcopy( data, &ipPacket, IPv4_HEADER_LENGTH );
            data = recvBuffer + ETHER_FRAME_HEADER_SIZE + IPv4_HEADER_LENGTH;
            bcopy( data, &icmpPacket, ICMP_HEADER_LENGTH );
            data = recvBuffer + ETHER_FRAME_HEADER_SIZE + IPv4_HEADER_LENGTH + ICMP_HEADER_LENGTH;
            bcopy( data, &icmpData, SMALL_MSG_SIZE );
            //printf("pid1(%d) pid2(%d)\n", getpid() & 0xffff, icmpPacket.icmp_id);
            char IPAddr[IP_SIZE];
            bzero( &IPAddr, IP_SIZE );
            Inet_ntop( AF_INET, &ipPacket.ip_dst, IPAddr, IP_SIZE );
            if( ( getpid() & 0xffff ) != icmpPacket.icmp_id )
            {
                if( 0 != bcmp( IPAddr, canonicalIPAddr, IP_SIZE ) )
                {
                    continue;
                }
                char temp[6];
                bzero( &temp, 6 );
                bcopy( recvBuffer, temp, 6 );
                bcopy( recvBuffer + 6, recvBuffer, 6 );
                bcopy( temp, recvBuffer + 6, 6 );
                Inet_pton( AF_INET, canonicalIPAddr, &ipPacket.ip_src );
                bcopy( &ipPacket, recvBuffer+ETHER_FRAME_HEADER_SIZE, IPv4_HEADER_LENGTH );
                NpInt32 send_result = sendto
                            (
                            pg,
                            recvBuffer,
                            ETH_FRAME_LEN,
                            0,
                            (SA *) &sa,
                            sizeof( sa )
                            );
                continue;
            }

            char ipAddr[IP_SIZE];
            bzero( &ipAddr, IP_SIZE );
            Inet_ntop( AF_INET, &ipPacket.ip_src, ipAddr, IP_SIZE );
            printf( "Received ICMP echo request from %s: byte=%d seq=%d ttl=%d\n", ipAddr, (NpInt32)(strlen(icmpData)), icmpPacket.icmp_seq, ipPacket.ip_ttl );
        }

        if( FD_ISSET( multicastRecvFd, &rset ) )
        {
            char recvBuffer[MAX_LINE];
            bzero( &recvBuffer, MAX_LINE );
            char idRequestPrefix[MAX_LINE];
            bzero( &idRequestPrefix, MAX_LINE );
            bcopy( "<<<<<(ID request)", idRequestPrefix, strlen( "<<<<<(ID request)" ) );
            char idReplyPrefix[MAX_LINE];
            bzero( &idReplyPrefix, MAX_LINE );
            bcopy( "<<<<<(ID reply)", idReplyPrefix, strlen( "<<<<<(ID reply)" ) );

            socklen_t len = sizeof( struct sockaddr );
            Recvfrom
                (
                multicastRecvFd,
                recvBuffer,
                MAX_LINE,
                0,
                sarecv,
                &len
                );
            if( 0 == bcmp( recvBuffer, idRequestPrefix, strlen( idRequestPrefix ) ) )
            {
                char thisHostNode[MAX_LINE];
                IPAddrToHostName( thisHostNode, canonicalIPAddr );
                printf( "Node %s. Received %s\n", thisHostNode, recvBuffer );

                // Send ID reply
                char idReplyString[MAX_LINE];
                bzero( &idReplyString, MAX_LINE );
                sprintf( idReplyString, "<<<<<(ID reply) Node %s. I am a member of the group.>>>>>", thisHostNode );
                Sendto( multicastSendFd, idReplyString, MAX_LINE, 0, sasend, salen);

                printf( "Node %s. Sending: %s\n", thisHostNode, idReplyString );
            }
            else if( 0 == bcmp( recvBuffer, idReplyPrefix, strlen( idReplyPrefix ) ) )
            {
                char thisHostNode[MAX_LINE];
                IPAddrToHostName( thisHostNode, canonicalIPAddr );
                printf( "Node %s. Received %s\n", thisHostNode, recvBuffer );
                idReplySent = TRUE;
            }

            StopPingging = TRUE;
        }

        if( 0 == nReady )
        {
            // Time out. Ping!
            if( TRUE == StartToPingging && FALSE == StopPingging && TRUE == lastNodeReached )
            {
                ++pingSentCountSinceLastNodeReached;
                if( MAX_PING_SENT == pingSentCountSinceLastNodeReached )
                {
                    StopPingging = TRUE;

                    char thisHostNode[MAX_LINE];
                    IPAddrToHostName( thisHostNode, canonicalIPAddr );
                    char idRequestString[MAX_LINE];
                    bzero( &idRequestString, MAX_LINE );
                    sprintf( idRequestString, "<<<<<(ID request) This is node %s. Tour has ended. Group members please identify yourselves.>>>>>", thisHostNode );
                    Sendto( multicastSendFd, idRequestString, MAX_LINE, 0, sasend, salen);

                    printf( "Node %s. Sending: %s\n", thisHostNode, idRequestString );
                }
            }

            if( TRUE == idReplySent )
            {
                printf( "Time (5 sec) is up. Terminating...\n" );
                exit( 1 );
            }

            if( TRUE == StartToPingging && FALSE == StopPingging )
            {
                struct sockaddr_in sa;
                bzero( &sa, sizeof(struct sockaddr_in) );
                sa.sin_family = AF_INET;
                Inet_pton( AF_INET, pingIPAddr, &(sa.sin_addr) );

                HwAddr hwAddr;
                areq( (SA *) &sa, sizeof(sa), &hwAddr );

                char *pStr = GetMainMacAddr( &netIfTable[0], numOfInterfaces );
                char macSuccinctAddr[MAC_SIZE];
                bzero( &macSuccinctAddr, MAC_SIZE );
                ToMACSuccinctFormat( macSuccinctAddr, pStr );
                NpInt32 ifIndex = GetMainInterface( &netIfTable[0], numOfInterfaces );

                PingNode( pg, canonicalIPAddr, macSuccinctAddr, ifIndex, pingIPAddr, &hwAddr );
            }
        }
    }

    return 0;
}

